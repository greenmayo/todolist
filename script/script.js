'use strict';

document.addEventListener('DOMContentLoaded',setup);

function setup(){    
    printToDoList();
    document.getElementById('form-task').addEventListener('submit',submit);
}

function submit(e){
/* This method takes the values from the form and save them in the 
Local storage memory*/
    let taskTitle = document.getElementById('task-name').value;
    let desc = document.getElementById('description').value;
    let impt = document.getElementById('importance').value;
    let rad =  document.getElementsByName('category');
    let cat = '';

    //checks what option is selected
    for(let i in rad){
        if (rad[i].checked)
        cat =rad[i].value;
    }

    //todo object
    let todo ={
        title: taskTitle,
        description: desc,
        importance: impt,
        category: cat
    };
   
    if(localStorage.getItem('toDo')=== null){
        let tasks = [];
        tasks.push(todo);
        localStorage.setItem('toDo',JSON.stringify(tasks));
    }else{
        let tasks = JSON.parse(localStorage.getItem('toDo'));
        tasks.push(todo);
        localStorage.setItem('toDo',JSON.stringify(tasks));
    }


    e.preventDefault();
}

function printToDoList(){
    let aside = document.getElementById('post');
    aside.innerHTML = ``;

    let tasks = JSON.parse(localStorage.getItem('toDo'));
    
    for(let i in tasks){
        let title = tasks[i].title;
        let description = tasks[i].description;
        let importance = getImportance(tasks[i].importance);
        let category = getCategory(tasks[i].category);

        aside.innerHTML += `
        <div id="${category}">
                    <div class="head-task">
                        <h3>${title}</h3>
                            <div class="imp-del">    
                                <p>${importance}</p>
                                <input type="button" value="Delete" class="dltbtn" id="deleteTask"/>
                            </div>
                    </div>
                        <p>${description}</p>
                </div>
        `
    }

}
function deleteLI(){
    addEventListener('click', function(e) {
        node.parentNode.removeChild(node);
    }, false);
}

function getCategory(category){
    if(category === 'School'){
        return 'tasks-school';
    }else if(category === 'Work'){
        return 'tasks-work';
    }else{
        return 'tasks-pers';
    }
}

function getImportance(importance){
    if(importance=== '1'){
        return '&#9733 &#9734 &#9734';
    }else if(importance ==='2'){
        return '&#9733 &#9733 &#9734'
    }else{
        return '&#9733 &#9733 &#9733'
    }
}